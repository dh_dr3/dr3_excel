%%%-------------------------------------------------------------------
%%% @author HuangYu
%%% @copyright (C) 2018, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 03. 七月 2018 下午5:59
%%%-------------------------------------------------------------------
-module(api_trans).
-author("HuangYu").

%% API
-export([trans/1]).

-spec trans(term()) -> ok | locked | {error, Where :: string(), Why :: term()}.
trans(TimeOut) ->
  case trans:pid() of
    Pid when is_pid(Pid) ->
      gen_server:call(Pid, trans, TimeOut);
    Err -> {error, "get pid", Err}
  end.