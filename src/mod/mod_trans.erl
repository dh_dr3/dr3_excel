%%%-------------------------------------------------------------------
%%% @author HuangYu
%%% @copyright (C) 2018, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 03. 七月 2018 下午1:42
%%%-------------------------------------------------------------------
-module(mod_trans).
-author("HuangYu").

%% API
-export([trans/1, pull_svn/1, trans_to_erl/1, copy/1, compile/1, scopy/1, update/1]).

%%将excel表导出成erlang文件；编译成beam；拷贝到目标服务器；调用逻辑服务器的接口，热更新配置文件
-spec trans(term()) -> ok | {error, Step :: string(), Reason :: string()}.
trans(ID) ->
  pull_svn(ID).

%%从svn拉取最新的资源。ID是用来区分每一次请求，可以用时间戳。
-spec pull_svn(term()) -> ok | {error, string(), string()}.
pull_svn(ID) ->
  SvnDir = app_ctl:get_cfg(svn_dir),
  {ok, Here} = file:get_cwd(),
  file:set_cwd(SvnDir),
  Rst = os:cmd("svn up"),
  lager:info("ID:~p,pull svn:~p", [ID, Rst]),
  file:set_cwd(Here),
  Updated = string:str(Rst, "Updated to revision") > 0,
  IsLatest = string:str(Rst, "At revision") > 0,
  case Updated orelse IsLatest of
    true -> trans_to_erl(ID);
    false -> {error, "pull_svn", Rst}
  end.

%%将excel转换成erlang
-spec trans_to_erl(term()) -> ok | {error, string(), string()}.
trans_to_erl(ID) ->
  PyDir = app_ctl:get_cfg(go_dir),
  {ok, Here} = file:get_cwd(),
  file:set_cwd(PyDir),
  Rst = os:cmd("./trexcel"),
  lager:info("ID:~p,trans to erl:~p", [ID, Rst]),
  file:set_cwd(Here),
  Success = app_ctl:get_cfg(ok),
  IsOk = string:str(Rst, Success) > 0,
  lager:info("Success:~p,isok:~p", [Success, IsOk]),
  case IsOk of
    true -> copy(ID);
    _ -> {eror, "trans_to_erl", Rst}
  end.

%%将导出后的erlang代码拷贝到编译工程中
-spec copy(term()) -> ok | {error, string(), string()}.
copy(ID) ->
  Erls = app_ctl:get_cfg(erl_dir),
  CompDir = app_ctl:get_cfg(comp_dir) ++ "/src",
  CpCmd = "cp " ++ Erls ++ "/*.erl " ++ CompDir,
  lager:info("cp cmd:~p", [CpCmd]),
  CpRst = os:cmd(CpCmd),
  lager:info("ID:~p,compile cp:~p", [ID, CpRst]),
  case CpRst of
    [] -> compile(ID);
    _ -> {error, "copy", CpRst}
  end.

%%将导出的erlang代码编译成beam
-spec compile(term()) -> ok | {error, string(), string()}.
compile(ID) ->
  CompDir = app_ctl:get_cfg(comp_dir),
  {ok, Here} = file:get_cwd(),
  file:set_cwd(CompDir),
  Rst = os:cmd("erlc -o ./ebin src/*.erl"),
  lager:info("ID:~p,compile erl:~p", [ID, Rst]),
  file:set_cwd(Here),
  Err1 = string:str(Rst, "ERR") > 0,
  Err2 = string:str(Rst, "err") > 0,
  lager:info("Err1:~p,Err2:~p", [Err1, Err2]),
  case Err1 andalso Err2 of
    true -> {error, "compile", Rst};
    _ -> scopy(ID)
  end.

%%将编译后的beam文件拷贝到目标机器
-spec scopy(term()) -> ok | {error, string(), string()}.
scopy(ID) ->
  Ebin = app_ctl:get_cfg(comp_dir) ++ "/ebin",
  Scp = app_ctl:get_cfg(scp),
  FileNum = os:cmd("ls -l | grep .beam | wc -l"),
  case scp(Scp, ID, Ebin, FileNum, ok) of
    ok -> update(ID);
    Err -> {error, "scopy", Err}
  end.

scp([], _, _, _, Rst) -> Rst;
scp([{Port, RemoteDir} | T], ID, Ebin, FileNum, Rst) ->
  ScpCmd = "scp -P " ++ Port ++ " " ++ Ebin ++ "/*.beam " ++ RemoteDir,
  lager:info("scp cmd:~p", [ScpCmd]),
  ScpRst = os:cmd(ScpCmd),
  lager:info("ID:~p,scp remote:~p,rst:~p", [ID, RemoteDir, ScpRst]),
  case ScpRst of
    [] -> scp(T, ID, Ebin, FileNum, Rst);
    _ -> {error, RemoteDir, "scp failed"}
  end.

%%调用远程逻辑服务器的接口，更新cfg代码
-spec update(term()) -> ok | {error, string(), string()}.
update(ID) ->
  Nodes = app_ctl:get_cfg(rpc_sub_nodes),
  UpTimeOut = app_ctl:get_cfg(up_timeout),
  up(Nodes, ID, UpTimeOut, ok).

up([], _, _, Rst) -> Rst;
up([Node | T], ID, UpTimeOut, Rst) ->
  case dhrpc:call(Node, api_update, cfgsup, [], UpTimeOut) of
    ok ->
      lager:info("ID:~p,update node:~p,success", [ID, Node]),
      up(T, ID, UpTimeOut, Rst);
    {error, Beam, Err} ->
      lager:info("ID:~p,update node:~p,beam:~p,error:~p", [ID, Node, Beam, Err]),
      {error, "update " ++ Beam, Err};
    Other ->
      {error, "update error", Other}
  end.