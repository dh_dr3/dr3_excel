%%%-------------------------------------------------------------------
%%% @author HuangYu
%%% @copyright (C) 2018, <COMPANY>
%%% @doc
%%%
%%% @end
%%% Created : 03. 七月 2018 上午11:04
%%%-------------------------------------------------------------------
-module(mod_ctl).
-author("HuangYu").

%% API
-export([]).

%% API
-export([]).
-export([
  init/0,
  before_upgrade/1,
  after_upgrade/1,
  stop/0,start_trans_sup/0]).

init() ->
  lager:info("dr3_excel start"),
  init_rpc(),
  start_trans_sup(),
  ok.

stop() ->
  dhtcp:stop(),
  supervisor:terminate_child(dr3_excel_sup, trans).

before_upgrade(Lv) ->
  lager:info("before_upgrade lv:~p", [Lv]),
  case Lv of
    "1" ->
      supervisor:terminate_child(dr3_excel_sup, trans),
      supervisor:delete_child(dr3_excel_sup, trans);
    "0" -> ok
  end.

after_upgrade(Lv) ->
  lager:info("after_upgrade lv:~p", [Lv]),
  case Lv of
    "1" ->
      start_trans_sup();
    "0" ->
      ok
  end.

init_rpc() ->
  ok = dhrpc:start(),
  ok = dhrpc:reg(),
  ok = dhrpc:subscribe_nodes(app_ctl:get_cfg(rpc_sub_nodes)),
  ok = dhrpc:subscribe_grps(app_ctl:get_cfg(rpc_sub_grps)).

start_trans_sup() ->
  ChildSpec = {trans, {trans, start_link, []},
               permanent, 60000, worker, [trans]},
  supervisor:start_child(dr3_excel_sup, ChildSpec).


